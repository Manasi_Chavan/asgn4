#include <stdio.h> 
#define MAX_FILE_NAME 100 
  
int main() 
{ 
    FILE *fp, *cp; 
    int count = 1;
    char filename[MAX_FILE_NAME]; 
    char c;  // To store a character read from file 
    printf("Enter file name: "); 
    scanf("%s", filename); 
  
    fp = fopen(filename, "r"); 
  	if (fp == NULL){ 
        printf("Could not open file %s", filename); 
        return 0; 
    } 
  	cp = fopen("dummy.txt","w");
   	fprintf(cp, "%d", count);  
    for (c = getc(fp); c != EOF; c = getc(fp)){ 
        fputc(c,cp);
        if (c == '\n'){ 
            count = count + 1;
            c = getc(fp);
            if (c != EOF){
            	fprintf(cp, "%d", count); 
   			}
   		}
    }
    fclose(fp); 
    printf("The file %s has %d lines\n ", filename, (count)); 
  
    return 0; 
} 